package com.example.springsecurityexample.models;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "users")
@Data
@ToString
public class User {

    @Id
    @Getter @Setter
    @Column(name = "username")
    private String userName;

    @Getter @Setter
    @Column(name = "password")
    private String password;

    @Getter @Setter
    @Column(name = "active")
    private boolean active;

    @Getter @Setter
    @Column(name = "roles")
    private String roles;
}
