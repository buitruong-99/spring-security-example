package com.example.springsecurityexample.resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeResource.class);

    @GetMapping("/")
    public String home() {
        LOGGER.trace(">>>>>> Accessing to home resource...");
        return ("<h1>Welcome</h1>");
    }

    @GetMapping("/user")
    public String user() {
        LOGGER.trace(">>>>>> Accessing to user resource...");
        return ("<h1>Welcome User</h1>");
    }

    @GetMapping("/admin")
    public String admin() {
        LOGGER.trace(">>>>>> Accessing to admin resource...");
        return ("<h1>Welcome Admin</h1>");
    }
}
